# PRACTICA 8 : Bibliografia con Listas Dobes

En esta práctica realizaremos la bibliografia con una implementación de listas dobles. El lenguaje que usaremos sera Ruby.
Para el desarrollo de la clase se utilizara una metodología de desarrollo dirigido por pruebas (TDD) y la herramienta RSpec.

## Herramientas de Test

En la práctica se implementa usando metodología de desarrollo dirigido por pruebas TDD, la herramienta usada es RSPec version 3.3.0

Para instalar la herramienta rspec se han usado los comandos: 

    $ bundle install rspec
    $ bundle exec rspec --init
   
Para ejecutar los tests se hace un rake, ya que en el Rakefile hay una tarea que llama a la herramienta rspec.
    $ rake


## Guard

Para ejecutar guard en la práctica sirve simplemente con el comando: 
    $ guard 

Se ha seguido el directorio completo de la práctica.

## Autor y Asignatura

Autor: Juan Ignacio Hita Manso
Asignatura: Lenguajes y paradigmas de la programación


